package org.bizone.parser.models

/**
 *
 * @param ipAddress the address for which the fact of multiple entry was detected
 * @param start the time of the first multiple login in the series
 * @param stop time of the last multiple login in the series
 * @param users list of [[LoginRecord]]s corresponding to the incident
 */
case class ResultRecord(
                       ipAddress: String,
                       start: Long,
                       stop: Long,
                       users: Seq[LoginRecord]
                       )
