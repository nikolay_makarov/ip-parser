package org.bizone.parser.models

/**
 * Describes event of user logging to the system.
 *
 * @param username user's login
 * @param ipAddress ip-address from which the system was logged in
 * @param timestamp unix timestamp of the event
 */
case class LoginRecord(
                        username: String,
                        ipAddress: String,
                        timestamp: Long
                      )
