package org.bizone.parser

import java.time.{Instant, LocalDateTime, ZoneId, ZoneOffset}
import java.time.format.DateTimeFormatter
import scala.language.implicitConversions

/**
 * Necessary conversions and handy function for core models.
 *
 * TODO: 1. switch to more reliable datetime library rather than java 2. Rework serialization
 */
package object models {

  val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-d HH:mm:ss")

  def stringToUnix(string: String): Long = {
    LocalDateTime.from(LocalDateTime.parse(string, dateTimeFormatter)).
      toEpochSecond(ZoneOffset.UTC)
  }

  def unixToString(timestamp: Long): String = {
    dateTimeFormatter.format(
      LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp), ZoneId.of("UTC"))
    )
  }

  def loginRecordFromString(sourceString: String): LoginRecord = {
    sourceString.trim.split(",").map(_.replaceAll("\"", "")) match {
      case Array(v1, v2, v3) => LoginRecord(
        v1,
        v2,
        stringToUnix(v3)
      )
      case _ => throw new RuntimeException(s"Failed to parse entry $sourceString into `LoginRecord`")
    }
  }

  /**
   * Prints [[ResultRecord]] according to task:
   *
   * IP address – адрес, для которого обнаружен факт множественного входа
   * Start – время первого множественного входа в серии
   * Stop – время последнего множественного входа в серии
   * Users – список имён пользователей и времени входа. Элементы списка разделены запятыми,
   * имя пользователя и время входя разделено двоеточием (':')
   *
   * @param record Logical result entry for Ip address
   * @return string entry
   */
  def resultRecordToString(record: ResultRecord) =
    s"\"${record.ipAddress}\",\"${unixToString(record.start)}\",\"${unixToString(record.stop)}\",\"${
      record.users.map(u => s"${u.username}:${unixToString(u.timestamp)}").mkString(" ")
    }\""

  def resultsToCsv(records: Iterable[ResultRecord]): Iterable[Byte] =
    records.map(x => resultRecordToString(x)).mkString("\n").getBytes()


  /**
   * Forms entry of output file based on batch of loginRecords from journal map
   *
   * @param ipAddress Ip Address of current login group
   * @param recordList list of [[LoginRecord]]s associated with given `ipAddress`
   * @return [[ResultRecord]] entry of output file
   */
  def resultRecordFromLoginRecordList(ipAddress: String, recordList: Seq[LoginRecord]): ResultRecord =
    ResultRecord(
      ipAddress = ipAddress,
      start = recordList.map(_.timestamp).min,
      stop = recordList.map(_.timestamp).max,
      users = recordList
    )

}
