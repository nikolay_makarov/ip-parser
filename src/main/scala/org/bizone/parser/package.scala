package org.bizone

import org.bizone.parser.models.{LoginRecord, ResultRecord, resultRecordFromLoginRecordList}


package object parser {

  /**
   * Keeps track of login records for given IP address.
   *
   * - If there were no logins for given address -> create such entry
   * - If there is a login present within range of timeout  or
   * if there are more than two logins present-> add login entry to collection
   * - If there is a login present out of timout range -> replace
   *
   *
   * @param acc accumulator map IP address to collection of [[LoginRecord]]s
   * @param rec new [[LoginRecord]]
   * @return Updated map
   */
  def updateWithNewLoginEvent(
                                     acc: Map[String, Seq[LoginRecord]],
                                     rec: LoginRecord,
                                     timeoutAsBigDec: BigDecimal
                                   ): Map[String, Seq[LoginRecord]] = {
    val timeout = timeoutAsBigDec.toLong
    val newEntry = acc.get(rec.ipAddress) match {
      case Some(presentSequence) if rec.timestamp - presentSequence.map(_.timestamp).max  < timeout =>
        rec.ipAddress -> (presentSequence :+ rec)
      case _ =>
        rec.ipAddress -> Seq(rec)
    }

    // filter out outdated single login events (ones that took place before rec.timestamp - timeout) to save space
    // and append new calculated entry
    acc.filterNot(e => e._2.size == 1 && e._2.head.timestamp < rec.timestamp - timeout) + newEntry
  }

  /**
   * Transforms aggregated map to result record
   */
  def journalMapToResult(journalMap: Map[String, Seq[LoginRecord]]): Iterable[ResultRecord] =
    journalMap.filter(_._2.length >= 2).map(e => resultRecordFromLoginRecordList(e._1, e._2))

}
