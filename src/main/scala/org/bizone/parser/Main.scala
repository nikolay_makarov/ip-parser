package org.bizone.parser

import org.bizone.parser.models.{LoginRecord, loginRecordFromString, resultsToCsv}
import zio.cli.HelpDoc.Span.text
import zio.cli._
import zio.stream.{ZPipeline, ZSink, ZStream}
import zio.{UIO, ZIOAppArgs, ZIOAppDefault}

import java.nio.file.Path

object Main extends ZIOAppDefault {

  val inputFileArg: Args[Path] = Args.file("in", Exists.Yes)
  val outputFileArg: Args[Path] = Args.file("out", Exists.No)
  // TODO: Fix to [Args.integer] when zio-cli allows integer arguments with custom names
  // Search timeout in seconds, defaults to 1h
  val timeoutArg: Args[BigDecimal] = Args.decimal("timeout")

  val args: Args[(Path, Path, BigDecimal)] = inputFileArg ++ outputFileArg ++ timeoutArg
  val cmd: Command[(Path, Path, BigDecimal)] = Command("bz-process",Options.none, args)

  val scan: (Path, Path, BigDecimal) => UIO[Unit] = {

    (inputPath, outputPath, timeout) => {
      // Process source login journal and transform it to CSV output
      zio.Console.printLine(s"Parsing $inputPath to $outputPath with timeout $timeout").! *>
      ZStream.fromIterableZIO(
        ZStream.fromFile(inputPath.toFile)
          .via(ZPipeline.utfDecode >>> ZPipeline.splitLines)
          .runFold(Map.empty[String, Seq[LoginRecord]]) { (acc, fileString) =>
            updateWithNewLoginEvent(acc, loginRecordFromString(fileString), timeout)
          }
          .map { journalMapToResult _ andThen resultsToCsv _ }
      )
        .run(ZSink.fromFile(outputPath.toFile))
        .orDie
        .unit
    }
  }

  val app: CliApp[Any, Nothing, (Path, Path, BigDecimal)] = CliApp.make(
    name = "BI.ZONE login inspector",
    version = "0.0.1",
    text("Scans login journal and discovers IP addresses with multiple login attempts during given timeout"),
    cmd
  )(scan.tupled)

  override def run =
    for {
      args <- ZIOAppArgs.getArgs
      _ <- zio.Console.printLine("Starting bizone-parser...")
      _ <- app.run(args.toList)
      _ <- zio.Console.printLine("Done")
    } yield ()

}
