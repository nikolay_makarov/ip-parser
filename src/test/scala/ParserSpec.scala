import org.bizone.parser.models.{LoginRecord, ResultRecord, resultRecordToString, stringToUnix, unixToString}
import org.scalatest.funsuite.AnyFunSuite

class ParserSpec extends AnyFunSuite {
  import ParserSpec._

  test("Timestamp conversion to and from unix works correctly") {
    val parsedFromStamp = stringToUnix(stamp)
    val parsedFromUnix = unixToString(unixLike)
    val sameAsStamp = unixToString(stringToUnix(stamp))

    assert(parsedFromStamp == unixLike)
    assert(parsedFromUnix == stamp)
    assert(sameAsStamp == stamp)
  }

  test("ResultRecord prints correctly") {
    assert(resultRecordToString(testRecord) == correctRecordStringRepr)
  }

}

object ParserSpec {

  private val ipAddr = "1.1.1.1"
  private val start = 1000
  private val stop = 2000
  private val stamp = "2015-11-30 23:11:40"
  private val unixLike: Long = 1448925100

  val testRecord = ResultRecord(ipAddr,
    start,
    stop,
    Seq(LoginRecord("u1", ipAddr, start), LoginRecord("u2", ipAddr, stop))
  )

  private val correctRecordStringRepr =
    "\"1.1.1.1\",\"1970-01-1 00:16:40\",\"1970-01-1 00:33:20\",\"u1:1970-01-1 00:16:40 u2:1970-01-1 00:33:20\""

}
