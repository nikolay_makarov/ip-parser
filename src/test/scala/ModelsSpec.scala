import org.bizone.parser.models.{LoginRecord, ResultRecord}
import org.bizone.parser.{journalMapToResult, updateWithNewLoginEvent}
import org.scalatest.funsuite.AnyFunSuite

class ModelsSpec extends AnyFunSuite {
  import ModelsSpec._

  test("journalToMapResult works correctly") {
    val result = journalMapToResult(testLoginRecordMap)

    assert(result.size == 1)
    assert(result == resJ)
  }
}

object ModelsSpec {

  private val ipAddress1 = "1.1.1.1"
  private val ipAddress2 = "2.2.2.2"
  private val start = 1000
  private val stop = 2000


  private val testLoginRecordMap = Map(
    ipAddress1 -> Seq(
      LoginRecord("u1", ipAddress1, start),
      LoginRecord("u2", ipAddress1, stop)
    ),
    ipAddress2 -> Seq(
      LoginRecord("u1", ipAddress2, start+stop)
    )
  )

  private val resJ = List(
    ResultRecord("1.1.1.1",1000,2000,List(LoginRecord("u1","1.1.1.1",1000), LoginRecord("u2","1.1.1.1",2000)))
  )

  private val correctResultRecord: ResultRecord = ResultRecord(
    ipAddress1,
    start,
    stop,
    Seq(
      LoginRecord("u1", ipAddress1, start),
      LoginRecord("u2", ipAddress1, stop)
    )
  )
}
