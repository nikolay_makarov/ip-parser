ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .settings(
    name := "bizone-parser"
  )


libraryDependencies += "dev.zio" %% "zio" % "2.0.0-RC6"
libraryDependencies += "dev.zio" %% "zio-streams" % "2.0.0-RC6"
libraryDependencies += "dev.zio" %% "zio-cli" % "0.2.6"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.12" % Test