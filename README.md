## Preparation:
 - `sbt assembly`
 - locate file to parse (input file) on your file system
 - make shure `/output/file/path` does not exists (we don't want any overwrites)

## Usage:
 `java -jar /path/to/jar /logins/path /output/file/path XYZ` where `XYZ` represents timeout in seconds

### To improve:
 - Add _helpdoc_
 - Errors and error handling
 - Parsing arguments with marker `-i` `-o`
 - Rework serialization: Write entries one-by-one instead of transforming thw whole pack to string

### Another approach:
Also, it is possible to solve the task using merge-sort on disk, sorting entries by ip and time, then applying the same
technique in batches. Such approach will reduce memory consumption, but increase disk load.